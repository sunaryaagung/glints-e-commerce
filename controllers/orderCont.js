const { Product } = require("../models/Product");
const { Order } = require("../models/Order");
const { Cart } = require("../models/Cart");

async function newOrder(req, res) {
  let product = await Product.findOne({ _id: req.body.product });
  /* istanbul ignore else*/
  if (!product || product.stock < 1) {
    return res.status(404).json({ message: "Out of stock" });
  }
  let cart = await Cart.findOne({ _user: req.user._id, status: false });
  /* istanbul ignore else*/
  if (!cart) {
    cart = new Cart({ _user: req.user._id });
    cart.save();
  }
  let order = new Order({
    _user: req.user._id,
    _product: req.body.product,
    qty: req.body.qty
  });

  product.stock -= order.qty;
  await product.save();
  await order.save();
  cart._orders.push(order._id);
  cart.totalPrice += product.price;
  cart.qty += order.qty;
  await cart.save();
  res.status(201).json({ cart: cart });
}

module.exports = { newOrder };
