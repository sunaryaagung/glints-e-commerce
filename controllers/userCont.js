const { User } = require("../models/User");
const bcyprt = require("bcryptjs");
const multer = require("multer");
const cloudinary = require("cloudinary");
const datauri = require("datauri");

async function get(req, res) {
  const user = await User.find();
  res.status(200).json({ data: user });
}

async function getById(req, res) {
  const user = await User.findById(req.params.id);
  if (!user) return res.status(404).json({ message: "Invalid user" });

  res.status(200).json({ data: user });
}

async function currentUser(req, res) {
  const user = await User.findById(req.user._id);
  res.status(200).json({ data: user });
}

async function add(req, res) {
  const { name, email, password, isMerchant } = req.body;
  let user = await User.findOne({ email: email });
  if (user) return res.status(409).json({ message: "User alrady exist" });
  user = new User({
    name,
    email,
    password,
    isMerchant
  });

  try {
    const salt = await bcyprt.genSalt(10);
    user.password = await bcyprt.hash(user.password, salt);
    await user.save();
    res.status(201).json({ data: user });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
}

async function update(req, res) {
  let newData = req.body;
  let user = await User.findOne({ email: newData.email });
  if (user)
    return res.status(409).json({ message: "User is already registered" });

  user = await User.findByIdAndUpdate(
    req.params.id,
    { $set: newData },
    { new: true }
  );
  res.status(200).json({ message: "User updated", data: user });
}

async function del(req, res) {
  const user = await User.findByIdAndRemove(req.params.id);
  if (!user) return res.status(404).json({ message: "User not found" });

  res.status(200).json({ message: "User has been deleted" });
}

async function pict(req, res) {
  const uploader = multer().single("image");
  var fileUp = req.file;

  if (!fileUp) {
    return res.status(428).json({
      message: "No file received"
    });
  } else {
    const dUri = new datauri();

    uploader(req, res, err => {
      var file = dUri.format(
        `${req.file.originalname}-${Date.now()}`,
        req.file.buffer
      );

      cloudinary.uploader
        .upload(file.content)
        .then(data => {
          User.findOneAndUpdate(
            { _id: req.user._id },
            { $set: { picture: data.secure_url } },
            { new: true }
          )
            .exec()
            .then(() => {
              return res.status(200).json({ message: "Uploaded" });
            });
        })
        .catch(err => {
          res.status(415).json({ message: err.message });
        });
    });
  }
}

async function login(req, res) {
  let { email, password } = req.body;
  if (!email || email === "" || !password || password === "") {
    return res.status(400).json({ message: "email and password is required" });
  }
  let user = await User.findOne({ email: email });
  if (!user)
    return res.status(400).json({ message: "Invalid user or password" });

  let validPassword = await bcyprt.compare(password, user.password);
  if (!validPassword)
    return res.status(400).json({ message: "Invalid user or password" });

  const token = user.generateToken();
  res.status(200).json({ token: token });
}

module.exports = { get, getById, add, login, update, del, pict, currentUser };
