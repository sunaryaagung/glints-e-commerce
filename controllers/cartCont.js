const { Cart } = require("../models/Cart");
const { Order } = require("../models/Order");
const { Product } = require("../models/Product");
const mongoose = require("mongoose");

async function myCart(req, res) {
  let cart = await Cart.findOne({
    _user: req.user._id,
    status: false
  }).populate({
    path: "_orders",
    select: { _product: 1, qty: 1 },
    populate: {
      path: "_product",
      select: { name: 1, price: 1 }
    }
  });
  if (!cart) {
    cart = {};
    return res.status(200).json({ data: cart });
  } else {
    res.status(200).json({ data: cart });
  }
}

async function history(req, res) {
  let cart = await Cart.find({ _user: req.user._id, status: true })
    .populate({
      path: "_orders",
      select: { _product: 1 },
      populate: {
        path: "_product",
        select: { name: 1, price: 1 }
      }
    })
    .sort("date");
  if (cart.length < 1)
    return res.status(404).json({ message: "no transaction" });
  res.status(200).json({ data: cart });
}

async function detail(req, res) {
  let cart = await Cart.findOne({
    _id: req.params.id,
    status: true
  }).populate({
    path: "_orders",
    select: { _product: 1, qty: 1 },
    populate: {
      path: "_product",
      select: { name: 1, price: 1 }
    }
  });
  if (!cart) return res.status(404).json({ message: "transaction not found" });
  res.status(200).json({ data: cart });
}

async function checkout(req, res) {
  let cart = await Cart.findOne({
    _user: req.user._id,
    status: false
  }).populate({
    path: "_orders",
    select: { _product: 1, qty: 1 },
    populate: {
      path: "_product",
      select: { name: 1, price: 1, stock: 1 }
    }
  });
  let prod = [];
  /* istanbul ignore next*/
  cart._orders.forEach(i => {
    prod.push(i._product);
  });
  /* istanbul ignore next*/
  let newProd = prod.filter(function(item) {
    console.log(item);
    return item.stock === 0;
  });
  /* istanbul ignore next*/
  if (newProd[0]) {
    return res.status(422).json({ message: "out of stock" });
  }
  cart.status = true;
  await cart.save();
  res.status(200).json({ prod: cart });
}

async function removeOrder(req, res) {
  try {
    let order = req.body.order;
    if (!mongoose.Types.ObjectId.isValid(order))
      return res.status(400).json({ message: "Invalid Id" });

    /* istanbul ignore next*/

    let cart = await Cart.findOne({
      _user: req.user._id,
      status: false
    }).populate("_orders");

    /* istanbul ignore next*/
    let cartNew = cart._orders.filter(function(item) {
      return item._id != order;
    });

    /* istanbul ignore next*/
    let qtyNew = cart._orders.filter(function(item) {
      return item._id == order;
    });

    /* istanbul ignore next*/
    let orderData = await Order.findById(order);

    /* istanbul ignore next*/
    let product = await Product.findById(orderData._product);

    /* istanbul ignore next*/
    product.stock += orderData.qty;

    /* istanbul ignore next*/
    await product.save();

    /* istanbul ignore next*/
    a = 0;

    /* istanbul ignore next*/
    qtyNew.forEach(element => {
      return (a = element.qty);
    });

    /* istanbul ignore next*/
    cart.qty -= a;

    /* istanbul ignore next*/
    cart._orders = cartNew;

    /* istanbul ignore next*/
    await cart.save();

    /* istanbul ignore next*/
    res.status(200).json({ data: cart });
  } catch (err) {
    /* istanbul ignore next*/
    res.status(422).json({ message: err.message });
  }
}

module.exports = { myCart, detail, history, checkout, removeOrder };
