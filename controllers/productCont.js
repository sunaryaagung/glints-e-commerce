const { Product } = require("../models/Product");
const { User } = require("../models/User");
const multer = require("multer");
const cloudinary = require("cloudinary");
const datauri = require("datauri");

async function getAll(req, res) {
  const products = await Product.find()
    .populate("_user", "name")
    .sort("name");
  res.status(200).json({ data: products });
}

async function getById(req, res) {
  const product = await Product.findById(req.params.id)
    .populate("_user", "name")
    .sort("name");
  res.status(200).json({ data: product });
}

async function getByUser(req, res) {
  const user = await User.findOne({ _id: req.user._id });
  const products = await Product.find({ _user: user._id })
    .populate("_user", "name")
    .sort("name");
  res.status(200).json({ data: products });
}

async function add(req, res) {
  let user = await User.findOne({ _id: req.user._id });
  if (user.isMerchant === false) {
    return res.status(403).json({ message: "Please create merchant Id" });
  } else {
    try {
      let product = await Product.findOne({
        name: req.body.name,
        _user: req.user._id
      });
      if (product) {
        product.stock += 1;
        await product.save();
        return res.status(200).json({ product });
      }

      const { name, price, stock } = req.body;
      product = new Product({
        name,
        price,
        stock,
        _user: user._id
      });

      await product.save();
      res.status(201).send(product);
    } catch (err) {
      res.status(400).json({ message: err.message });
    }
  }
}

async function updateProduct(req, res) {
  let newData = req.body;
  let product = await Product.findOneAndUpdate(
    { _id: req.params.id, _user: req.user._id },
    { $set: newData },
    { new: true }
  );
  if (!product) return res.status(404).json({ message: "Invalid product" });
  res.status(200).json({ message: "Product updated", data: product });
}

async function delProduct(req, res) {
  const product = await Product.findOneAndRemove({
    _id: req.params.id,
    _user: req.user._id
  });
  if (!product) return res.status(404).json({ message: "Invalid User" });

  res.status(200).json({ message: "Product has been deleted" });
}

async function prodPict(req, res) {
  const uploader = multer().single("image");
  var fileUp = req.file;

  if (!fileUp) {
    return res.status(428).json({
      message: "No file received"
    });
  } else {
    const dUri = new datauri();

    uploader(req, res, err => {
      var file = dUri.format(
        `${req.file.originalname}-${Date.now()}`,
        req.file.buffer
      );

      cloudinary.uploader
        .upload(file.content)
        .then(data => {
          Product.findOneAndUpdate(
            { _id: req.params.id, _user: req.user._id },
            { $set: { picture: data.secure_url } },
            { new: true }
          )
            .exec()
            .then(() => {
              return res.status(200).json({ message: "Uploaded" });
            });
        })
        .catch(err => {
          res.status(415).json({ message: err.message });
        });
    });
  }
}

module.exports = {
  getAll,
  getByUser,
  add,
  updateProduct,
  delProduct,
  prodPict,
  getById
};
