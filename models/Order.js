const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
  _user: { type: mongoose.Schema.Types.ObjectId, ref: "users", required: true },
  _product: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "products",
    required: true
  },
  qty: { type: Number, default: 1, min: 1 },
  dateOrder: { type: Date, default: Date.now() }
});

const Order = new mongoose.model("orders", orderSchema);

exports.Order = Order;
