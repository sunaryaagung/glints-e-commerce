const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
  _user: { type: mongoose.Schema.Types.ObjectId, ref: "users" },
  _orders: [{ type: mongoose.Schema.Types.ObjectId, ref: "orders" }],
  qty: { type: Number, default: 0 },
  totalPrice: { type: Number, default: 0, min: 0 },
  status: { type: Boolean, default: false },
  dateOut: Date
});

const Cart = new mongoose.model("carts", cartSchema);

exports.Cart = Cart;
