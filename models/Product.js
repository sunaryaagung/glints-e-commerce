const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
  name: { type: String, required: true },
  price: { type: Number, min: 1, required: true },
  stock: { type: Number, min: 0, required: true },
  _user: { type: mongoose.Schema.Types.ObjectId, ref: "users" },
  picture: String
});

const Product = new mongoose.model("products", productSchema);

exports.Product = Product;
