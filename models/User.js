const mongoose = require("mongoose");
require("mongoose-type-email");
const jwt = require("jsonwebtoken");
require("dotenv").config();

const userSchema = new mongoose.Schema({
  name: { type: String, required: true },
  email: { type: mongoose.SchemaTypes.Email, required: true },
  password: { type: String, required: true },
  isMerchant: { type: Boolean, default: false },
  picture: String
});

userSchema.methods.generateToken = function() {
  const token = jwt.sign({ _id: this._id }, process.env.SECRET_KEY);
  return token;
};

const User = new mongoose.model("users", userSchema);

exports.User = User;
