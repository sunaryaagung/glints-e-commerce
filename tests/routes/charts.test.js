process.env.NODE_ENV = "test";

const { User } = require("../../models/User");
const { Cart } = require("../../models/Cart");

const bcrypt = require("bcryptjs");
const chai = require("chai");
const chaihttp = require("chai-http");
const server = require("../../index");
const should = chai.should();
const faker = require("faker");
const fs = require("fs");

chai.use(chaihttp);

let randomEmail = faker.internet.email();
let randomName = faker.name.findName();
let fakeUser = {
  name: "fake",
  email: "fake@mail.com",
  password: "12345",
  isMerchant: true
};
let fakeId = "5d8311d7591c4c618a45f675";

function login() {
  let user = new User(fakeUser);
  user.save();
  let token = user.generateToken();
  let bearer = `bearer ${token}`;
  let _user = user._id;
  return { bear: bearer, id: _user };
}

//PARENT BLOCK
describe("Chart", () => {
  beforeEach(done => {
    User.deleteMany({}, { new: true }, err => {
      Cart.deleteMany({}, { new: true }, err => {
        done();
      });
    });
  });
  describe("/GET Chart", () => {
    it("should return cart empty chart is user cart is empty", done => {
      let usr = login();
      chai
        .request(server)
        .get("/api/cart")
        .set("Authorization", usr.bear)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.an("object");
          done();
        });
    });
  });

  describe("/GET Chart", () => {
    it("should return cart by the given user ", done => {
      let usr = login();
      let cart = new Cart({ _user: usr.id, status: false, _orders: fakeId });
      cart.save((err, res) => {
        chai
          .request(server)
          .get("/api/cart")
          .set("Authorization", usr.bear)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.an("object");
            done();
          });
      });
    });
  });

  describe("/GET Chart", () => {
    it("should return order history ", done => {
      let usr = login();
      let cart = new Cart({ _user: usr.id, status: true, _orders: fakeId });
      cart.save((err, res) => {
        chai
          .request(server)
          .get("/api/cart/history")
          .set("Authorization", usr.bear)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.an("object");
            done();
          });
      });
    });
  });

  describe("/GET Chart", () => {
    it("should return no transaction if there is no order history ", done => {
      let usr = login();
      let cart = new Cart({ _user: usr.id });
      cart.save((err, res) => {
        chai
          .request(server)
          .get("/api/cart/history")
          .set("Authorization", usr.bear)
          .end((err, res) => {
            res.should.have.status(404);
            res.body.should.be.an("object");
            done();
          });
      });
    });
  });

  describe("/GET Chart", () => {
    it("should return err if no transaction found ", done => {
      let usr = login();
      let cart = new Cart({ _user: usr.id });
      cart.save((err, res) => {
        chai
          .request(server)
          .get("/api/cart/detail/" + cart._id)
          .set("Authorization", usr.bear)
          .end((err, res) => {
            res.should.have.status(404);
            res.body.should.be.an("object");
            done();
          });
      });
    });
  });

  describe("/GET Chart", () => {
    it("should return order detail ", done => {
      let usr = login();
      let cart = new Cart({ _user: usr.id, status: true, _orders: fakeId });
      cart.save((err, res) => {
        chai
          .request(server)
          .get("/api/cart/detail/" + cart._id)
          .set("Authorization", usr.bear)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.an("object");
            done();
          });
      });
    });
  });

  describe("/GET Chart", () => {
    it("should return order detail ", done => {
      let usr = login();
      let cart = new Cart({ _user: usr.id, status: false });
      cart.save((err, res) => {
        chai
          .request(server)
          .put("/api/cart/checkout")
          .set("Authorization", usr.bear)
          .send({ status: true, dateOut: Date.now() })
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.an("object");
            done();
          });
      });
    });
  });

  describe("/Remove Chart", () => {
    it("should return error if object id is not valid ", done => {
      let usr = login();
      chai
        .request(server)
        .delete("/api/cart/remove")
        .set("Authorization", usr.bear)
        .send({ order: "1" })
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });

  describe("/Remove Chart", () => {
    it("should return error 422 if something fail ", done => {
      let usr = login();
      chai
        .request(server)
        .delete("/api/cart/remove")
        .set("Authorization", usr.bear)
        .send({ order: 1 })
        .end((err, res) => {
          res.should.have.status(422);
          done();
        });
    });
  });
});
