process.env.NODE_ENV = "test";

const mongoose = require("mongoose");
const { Product } = require("../../models/Product");
const { User } = require("../../models/User");

const bcrypt = require("bcryptjs");
const chai = require("chai");
const chaihttp = require("chai-http");
const server = require("../../index");
const should = chai.should();
const faker = require("faker");
const fs = require("fs");

chai.use(chaihttp);

let randomEmail = faker.internet.email();
let randomName = faker.name.findName();
let fakeUser = {
  name: "fake",
  email: "fake@mail.com",
  password: "12345",
  isMerchant: true
};
let fakeId = "5d8129d830374d0016769548";

function login() {
  let user = new User(fakeUser);
  user.save();
  let token = user.generateToken();
  let bearer = `bearer ${token}`;
  let _user = user._id;
  return { bear: bearer, id: _user };
}

//PARENT BLOCK
describe("Product", () => {
  beforeEach(done => {
    Product.deleteMany({}, { new: true }, err => {
      done();
    });
  });
  describe("/GET product", () => {
    it("it should return all product in db", done => {
      chai
        .request(server)
        .get("/api/products")
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });
  });

  describe("/POST product", () => {
    it("it should return product with the give user", done => {
      let usr = login();
      let product = new Product({
        name: "test product",
        price: 20,
        stock: 20,
        _user: usr.id
      });
      product.save((err, res) => {
        chai
          .request(server)
          .get("/api/products/detail/" + product._id)
          .set("Authorization", usr.bear)
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/GET product", () => {
    it("it should return my product", done => {
      let { bear, id } = login();
      chai
        .request(server)
        .get("/api/products/myProducts")
        .set("Authorization", bear)
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });
  });

  describe("/POST product", () => {
    it("it should return 403 if user is not merchant", done => {
      let user = new User({
        name: "test",
        email: "test@mail.com",
        password: "12345"
      });
      let token = user.generateToken();
      let bearer = `bearer ${token}`;
      user.save((err, res) => {
        chai
          .request(server)
          .post("/api/products")
          .set("Authorization", bearer)
          .end((err, res) => {
            res.should.have.status(403);
            done();
          });
      });
    });
  });

  describe("/POST product", () => {
    it("it should add product to db", done => {
      let usr = login();
      chai
        .request(server)
        .post("/api/products")
        .set("Authorization", usr.bear)
        .send({ name: "test product", price: 20, stock: 20, _user: usr.id })
        .end((err, res) => {
          res.should.have.status(201);
          done();
        });
    });
  });

  describe("/POST product", () => {
    it("it should add more stock to product", done => {
      let usr = login();
      let produk = new Product({
        name: "test product",
        price: 20,
        stock: 20,
        _user: usr.id
      });

      produk.save((err, res) => {
        chai
          .request(server)
          .post("/api/products")
          .set("Authorization", usr.bear)
          .send({ name: "test product", price: 20, stock: 20, _user: usr.id })
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/POST product", () => {
    it("it should return error if req body is bad", done => {
      let usr = login();
      chai
        .request(server)
        .post("/api/products")
        .set("Authorization", usr.bear)
        .send({ name: "test product", price: 20 })
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });

  describe("/PUT product", () => {
    it("it update a product", done => {
      let usr = login();
      let produk = new Product({
        name: "test product",
        price: 20,
        stock: 20,
        _user: usr.id
      });
      produk.save((err, res) => {
        chai
          .request(server)
          .put("/api/products/" + produk._id)
          .set("Authorization", usr.bear)
          .send({ name: "edited product", price: 20, stock: 20, _user: usr.id })
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/PUT product", () => {
    it("it return error if product not found", done => {
      let usr = login();
      chai
        .request(server)
        .put("/api/products/" + fakeId)
        .set("Authorization", usr.bear)
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
    });
  });

  describe("/DELETE product", () => {
    it("it delete produk with the given id", done => {
      let usr = login();
      let produk = new Product({
        name: "test product",
        price: 20,
        stock: 20,
        _user: usr.id
      });

      produk.save((err, res) => {
        chai
          .request(server)
          .delete("/api/products/" + produk._id)
          .set("Authorization", usr.bear)
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/DELETE product", () => {
    it("it should return error product not found", done => {
      let usr = login();
      chai
        .request(server)
        .delete("/api/products/" + fakeId)
        .set("Authorization", usr.bear)
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
    });
  });

  describe("/PUT picture", () => {
    it("it should update product picture", done => {
      let usr = login();
      let file = "/home/arya/binar/glints-e-commerce/tests/routes/icon.png";
      let produk = new Product({
        name: "test product",
        price: 20,
        stock: 20,
        _user: usr.id
      });
      produk.save((err, res) => {
        chai
          .request(server)
          .put("/api/products/pict/" + produk._id)
          .set("Authorization", usr.bear)
          .attach("file", fs.readFileSync(`${file}`), "icon.png")
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/PUT picture", () => {
    it("it return error 428 if no file provided", done => {
      let usr = login();
      let produk = new Product({
        name: "test product",
        price: 20,
        stock: 20,
        _user: usr.id
      });
      produk.save((err, res) => {
        chai
          .request(server)
          .put("/api/products/pict/" + produk._id)
          .set("Authorization", usr.bear)
          .end((err, res) => {
            res.should.have.status(428);
            done();
          });
      });
    });
  });

  describe("/PUT picture", () => {
    it("it should return error 415 if file is not an image", done => {
      let usr = login();
      let bfile = "/home/arya/binar/glints-e-commerce/index.js";
      let produk = new Product({
        name: "test product",
        price: 20,
        stock: 20,
        _user: usr.id
      });
      produk.save((err, res) => {
        chai
          .request(server)
          .put("/api/products/pict/" + produk._id)
          .set("Authorization", usr.bear)
          .attach("file", fs.readFileSync(`${bfile}`), "icon.png")
          .end((err, res) => {
            res.should.have.status(415);
            done();
          });
      });
    });
  });
});
