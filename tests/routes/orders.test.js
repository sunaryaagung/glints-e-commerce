process.env.NODE_ENV = "test";

const mongoose = require("mongoose");
const { Product } = require("../../models/Product");
const { User } = require("../../models/User");
const { Cart } = require("../../models/Cart");
const { Order } = require("../../models/Order");

const bcrypt = require("bcryptjs");
const chai = require("chai");
const chaihttp = require("chai-http");
const server = require("../../index");
const should = chai.should();
const faker = require("faker");
const fs = require("fs");

chai.use(chaihttp);

let randomEmail = faker.internet.email();
let randomName = faker.name.findName();
let fakeUser = {
  name: "fake",
  email: "fake@mail.com",
  password: "12345",
  isMerchant: true
};
let fakeId = "5d8129d830374d0016769548";

function login() {
  let user = new User(fakeUser);
  user.save();
  let token = user.generateToken();
  let bearer = `bearer ${token}`;
  let _user = user._id;
  return { bear: bearer, id: _user };
}

//PARENT BLOCK
describe("Order", () => {
  beforeEach(done => {
    Order.deleteMany({}, { new: true }, err => {
      done();
    });
  });

  describe("/POST order", () => {
    it("it should return error if product is out of stock", done => {
      let usr = login();
      let produk = new Product({
        name: "test product",
        price: 20,
        stock: 0,
        _user: usr.id
      });
      produk.save((err, res) => {
        chai
          .request(server)
          .post("/api/orders")
          .set("Authorization", usr.bear)
          .send({ product: produk._id })
          .end((err, res) => {
            res.should.have.status(404);
            done();
          });
      });
    });
  });

  describe("/POST order", () => {
    it("it should add order to stock", done => {
      let usr = login();
      let produk = new Product({
        name: "test product",
        price: 20,
        stock: 10,
        _user: usr.id
      });
      produk.save((err, res) => {
        chai
          .request(server)
          .post("/api/orders")
          .set("Authorization", usr.bear)
          .send({ product: produk._id })
          .end((err, res) => {
            res.should.have.status(201);
            done();
          });
      });
    });
  });
});
