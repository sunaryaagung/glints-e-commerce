process.env.NODE_ENV = "test";

const mongoose = require("mongoose");
const { User } = require("../../models/User");

const bcrypt = require("bcryptjs");
const chai = require("chai");
const chaihttp = require("chai-http");
const server = require("../../index");
const should = chai.should();
const faker = require("faker");
const fs = require("fs");

chai.use(chaihttp);

let randomEmail = faker.internet.email();
let randomName = faker.name.findName();
let fakeUser = { name: "fake", email: "fake@mail.com", password: "12345" };
let fakeId = "5d8129d830374d0016769548";

//PARENT BLOCK
describe("User", () => {
  beforeEach(done => {
    User.deleteMany({}, { new: true }, err => {
      done();
    });
  });

  //test get route
  describe("/GET user", () => {
    it("it should return list of user", done => {
      chai
        .request(server)
        .get("/api/users")
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });
  });

  describe("/POST user", () => {
    it("it should add new user to db", done => {
      let user = { name: randomName, email: randomEmail, password: "12345" };
      chai
        .request(server)
        .post("/api/users")
        .send(user)
        .end((err, res) => {
          res.should.have.status(201);
          res.should.be.an("object");
          done();
        });
    });
  });

  describe("/POST user", () => {
    it("it should return error if req body is bad", done => {
      let user = { email: randomEmail, password: "12345" };
      chai
        .request(server)
        .post("/api/users")
        .send(user)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });

  describe("/POST user", () => {
    it("it should return error if user is already exist", done => {
      let user = new User(fakeUser);
      user.save(() => {
        chai
          .request(server)
          .post("/api/users")
          .send(fakeUser)
          .end((err, res) => {
            res.should.have.status(409);
            done();
          });
      });
    });
  });

  describe("/Login user", () => {
    it("it should have status 200 if user login with the righ data", done => {
      let salt = bcrypt.genSaltSync(10);
      let user = new User(fakeUser);
      user.password = bcrypt.hashSync(user.password, salt);
      user.save((err, res) => {
        chai
          .request(server)
          .post("/api/users/login")
          .send({ email: fakeUser.email, password: fakeUser.password })
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/Login user", () => {
    it("it return error if body is bad", done => {
      let salt = bcrypt.genSaltSync(10);
      let user = new User(fakeUser);
      user.password = bcrypt.hashSync(user.password, salt);
      user.save((err, res) => {
        chai
          .request(server)
          .post("/api/users/login")
          .end((err, res) => {
            res.should.have.status(400);
            done();
          });
      });
    });
  });

  describe("/Login user", () => {
    it("it return error if email is wrong", done => {
      let salt = bcrypt.genSaltSync(10);
      let user = new User(fakeUser);
      user.password = bcrypt.hashSync(user.password, salt);
      user.save((err, res) => {
        chai
          .request(server)
          .post("/api/users/login")
          .send({ email: "1", password: fakeUser.password })
          .end((err, res) => {
            res.should.have.status(400);
            done();
          });
      });
    });
  });

  describe("/Login user", () => {
    it("it return error if password is wrong", done => {
      let salt = bcrypt.genSaltSync(10);
      let user = new User(fakeUser);
      user.password = bcrypt.hashSync(user.password, salt);
      user.save((err, res) => {
        chai
          .request(server)
          .post("/api/users/login")
          .send({ email: fakeUser.email, password: "1" })
          .end((err, res) => {
            res.should.have.status(400);
            done();
          });
      });
    });
  });

  describe("/Get user by ID", () => {
    it("it should return user with the given id", done => {
      let user = new User(fakeUser);
      let token = user.generateToken();
      let bearer = `bearer ${token}`;
      user.save((err, res) => {
        chai
          .request(server)
          .get("/api/users/" + user._id)
          .set("Authorization", bearer)
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/Get user by ID", () => {
    it("it should return err if params is not object id", done => {
      let user = new User(fakeUser);
      let token = user.generateToken();
      let bearer = `bearer ${token}`;
      user.save((err, res) => {
        chai
          .request(server)
          .get("/api/users/1")
          .set("Authorization", bearer)
          .end((err, res) => {
            res.should.have.status(400);
            done();
          });
      });
    });
  });

  describe("/Get user by ID", () => {
    it("it should return err if user object id is wrong", done => {
      let user = new User(fakeUser);
      let token = user.generateToken();
      let bearer = `bearer ${token}`;
      user.save((err, res) => {
        chai
          .request(server)
          .get("/api/users/" + fakeId)
          .set("Authorization", bearer)
          .end((err, res) => {
            res.should.have.status(404);
            done();
          });
      });
    });
  });

  describe("/Get current user", () => {
    it("it should return current login user", done => {
      let user = new User(fakeUser);
      let token = user.generateToken();
      let bearer = `bearer ${token}`;
      user.save((err, res) => {
        chai
          .request(server)
          .get("/api/users/me")
          .set("Authorization", bearer)
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/PUT user", () => {
    it("it return error if edited user already exist", done => {
      let user = new User(fakeUser);
      let token = user.generateToken();
      let bearer = `bearer ${token}`;
      user.save((err, res) => {
        chai
          .request(server)
          .put("/api/users/" + user._id)
          .set("Authorization", bearer)
          .send(fakeUser)
          .end((err, res) => {
            res.should.have.status(409);
            done();
          });
      });
    });
  });

  describe("/PUT user", () => {
    it("it should edit user in db", done => {
      let user = new User(fakeUser);
      let token = user.generateToken();
      let bearer = `bearer ${token}`;
      user.save((err, res) => {
        chai
          .request(server)
          .put("/api/users/" + user._id)
          .set("Authorization", bearer)
          .send({ name: "edited" })
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/DELETE user", () => {
    it("it should return err if user is not found", done => {
      let user = new User(fakeUser);
      let token = user.generateToken();
      let bearer = `bearer ${token}`;
      user.save((err, res) => {
        chai
          .request(server)
          .delete("/api/users/" + fakeId)
          .set("Authorization", bearer)
          .end((err, res) => {
            res.should.have.status(404);
            done();
          });
      });
    });
  });

  describe("/DELETE user", () => {
    it("it should delete user in db", done => {
      let user = new User(fakeUser);
      let token = user.generateToken();
      let bearer = `bearer ${token}`;
      user.save((err, res) => {
        chai
          .request(server)
          .delete("/api/users/" + user._id)
          .set("Authorization", bearer)
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });
  describe("/PUT picture", () => {
    it("it should update user picture", done => {
      let user = new User(fakeUser);
      let token = user.generateToken();
      let bearer = `bearer ${token}`;
      let file =
        "/home/arya/binar/glints-e-commerce/tests/routes/icon.png";
      user.save((err, res) => {
        chai
          .request(server)
          .put("/api/users/pict")
          .set("Authorization", bearer)
          .attach("file", fs.readFileSync(`${file}`), "icon.png")
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/PUT picture", () => {
    it("it should return status 428 if there is no pict", done => {
      let user = new User(fakeUser);
      let token = user.generateToken();
      let bearer = `bearer ${token}`;
      user.save((err, res) => {
        chai
          .request(server)
          .put("/api/users/pict")
          .set("Authorization", bearer)
          .end((err, res) => {
            res.should.have.status(428);
            done();
          });
      });
    });
  });

  describe("/PUT picture", () => {
    it("it should return status 415 if file format is wrong", done => {
      let user = new User(fakeUser);
      let token = user.generateToken();
      let bearer = `bearer ${token}`;
      let bfile = "/home/arya/binar/glints-e-commerce/index.js";
      user.save((err, res) => {
        chai
          .request(server)
          .put("/api/users/pict")
          .set("Authorization", bearer)
          .attach("file", fs.readFileSync(`${bfile}`), "index.js")
          .end((err, res) => {
            res.should.have.status(415);
            done();
          });
      });
    });
  });
});
