process.env.NODE_ENV = "test";

const { User } = require("../../models/User");

const bcrypt = require("bcryptjs");
const chai = require("chai");
const chaihttp = require("chai-http");
const server = require("../../index");
const should = chai.should();
const faker = require("faker");
const fs = require("fs");

chai.use(chaihttp);

let randomEmail = faker.internet.email();
let randomName = faker.name.findName();
let fakeUser = {
  name: "fake",
  email: "fake@mail.com",
  password: "12345",
  isMerchant: true
};
let fakeId = "5d8129d830374d0016769548";

function login() {
  let user = new User(fakeUser);
  user.save();
  let token = user.generateToken();
  let bearer = `bearer ${token}`;
  let _user = user._id;
  return { bear: bearer, id: _user };
}

//Parent Block
describe("Auth", () => {
  beforeEach(done => {
    User.deleteMany({}, { new: true }, err => {
      done();
    });
  });

  describe("/Auth", () => {
    it("it should return error if user is not logged in", done => {
      chai
        .request(server)
        .get("/api/users/me")
        .end((err, res) => {
          res.should.have.status(401);
          done();
        });
    });
  });

  describe("/Auth", () => {
    it("it should return error user is not found", done => {
      let user = new User(fakeUser);
      let token = user.generateToken();
      let bearer = `bearer ${token}`;
      chai
        .request(server)
        .get("/api/users/me")
        .set("Authorization", bearer)
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
    });
  });

  describe("/Auth", () => {
    it("it should return error if token is invalid", done => {
      let usr = login();
      chai
        .request(server)
        .get("/api/users/me")
        .set("Authorization", usr.id)
        .end((err, res) => {
          res.should.have.status(401);
          done();
        });
    });
  });
});
