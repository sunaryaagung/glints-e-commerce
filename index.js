/* istanbul ignore file */

const express = require("express");
const app = express();
const router = require("./routers");
const cors = require("cors");

require("./config/db")();

const port = process.env.PORT || 3000;

app.use(cors());
app.use(express.json());
app.use("/api", router);
app.get("/", (req, res) => {
  res.send({ Hy: "There" });
});

app.listen(port, () => console.log(`Connected to ${port}`));

module.exports = app;
