const express = require("express");
const router = express.Router();
const auth = require("../middleware/auth");
const objectId = require("../middleware/objectId");
const {
  myCart,
  detail,
  history,
  checkout,
  removeOrder
} = require("../controllers/cartCont");

router.get("/", auth, myCart);
router.put("/checkout", auth, checkout);
router.get("/detail/:id", auth, objectId, detail);
router.get("/history", auth, history);
router.delete("/remove", auth, removeOrder);

module.exports = router;
