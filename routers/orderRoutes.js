const express = require("express");
const router = express.Router();
const auth = require("../middleware/auth");
const { newOrder } = require("../controllers/orderCont");

router.post("/", auth, newOrder);

module.exports = router;
