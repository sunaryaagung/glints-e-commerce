const express = require("express");
const router = express.Router();
const user = require("./userRoutes");
const product = require("./productRoutes");
const order = require("./orderRoutes");
const cart = require("./cartRoutes");

router.use("/users", user);
router.use("/products", product);
router.use("/orders", order);
router.use("/cart", cart);
module.exports = router;
