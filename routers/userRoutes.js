const express = require("express");
const router = express.Router();
const auth = require("../middleware/auth");
const cloudinaryConfig = require("../config/cloudinaryConfig");
const objectId = require("../middleware/objectId");
const upload = require("../middleware/multer");
const {
  get,
  add,
  login,
  getById,
  update,
  del,
  pict,
  currentUser
} = require("../controllers/userCont");

router.use("*", cloudinaryConfig);

router.get("/", get);
router.get("/me", auth, currentUser);
router.get("/:id", auth, objectId, getById);
router.post("/", add);
router.put("/pict", auth, upload.single("file"), pict);
router.post("/login", login);
router.put("/:id", auth, objectId, update);
router.delete("/:id", del);

module.exports = router;
