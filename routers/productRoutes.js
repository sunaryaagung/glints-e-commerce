const express = require("express");
const router = express.Router();
const auth = require("../middleware/auth");
const objectId = require("../middleware/objectId");
const cloudinaryConfig = require("../config/cloudinaryConfig");
const upload = require("../middleware/multer");

const {
  getAll,
  getByUser,
  add,
  updateProduct,
  delProduct,
  prodPict,
  getById
} = require("../controllers/productCont");

router.use("*", cloudinaryConfig);

router.get("/", getAll);
router.get("/detail/:id", getById);
router.get("/myProducts", auth, getByUser);
router.post("/", auth, add);
router.put("/pict/:id", auth, upload.single("file"), prodPict);
router.put("/:id", auth, objectId, updateProduct);
router.delete("/:id", auth, objectId, delProduct);

module.exports = router;
